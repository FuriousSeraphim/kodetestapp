package kode.kinopoisk.bakurov.models

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.*
import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping
import com.pushtorefresh.storio.sqlite.operations.delete.DefaultDeleteResolver
import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver
import com.pushtorefresh.storio.sqlite.operations.put.DefaultPutResolver
import com.pushtorefresh.storio.sqlite.queries.*
import java.lang.reflect.Type

data class City(val id: Int, val name: String) : Parcelable {
    companion object {
        @JvmStatic fun deserializer() = Deserializer()

        @JvmStatic fun createTable(database: SQLiteDatabase) {
            database.execSQL("CREATE TABLE ${City.Table.TABLE_NAME} (" +
                    "${City.Table.ID} INTEGER PRIMARY KEY," +
                    "${City.Table.NAME} TEXT" +
                    ");")
        }

        @JvmStatic fun storioTypeMapping() = SQLiteTypeMapping.builder<City>()
                .putResolver(SimplePutResolver())
                .getResolver(SimpleGetResolver())
                .deleteResolver(SimpleDeleteResolver())
                .build()

        @JvmField val CREATOR = object : Parcelable.Creator<City> {
            override fun createFromParcel(source: Parcel) = City(source.readInt(), source.readString())
            override fun newArray(size: Int): Array<City?> = arrayOfNulls(size)
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(id)
        dest.writeString(name)
    }

    class Deserializer : JsonDeserializer<City> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext) = json.asJsonObject.let {
            City(it["id"].asInt, it["name"].asString)
        }
    }

    class SimplePutResolver : DefaultPutResolver<City>() {
        override fun mapToInsertQuery(entity: City) = InsertQuery.builder()
                .table(City.Table.TABLE_NAME)
                .build()

        override fun mapToUpdateQuery(entity: City) = UpdateQuery.builder()
                .table(City.Table.TABLE_NAME)
                .where("${City.Table.ID} = ${entity.id}")
                .build()

        override fun mapToContentValues(entity: City) = ContentValues().apply {
            put(City.Table.ID, entity.id)
            put(City.Table.NAME, entity.name)
        }
    }

    class SimpleGetResolver : DefaultGetResolver<City>() {
        override fun mapFromCursor(cursor: Cursor) = City(
                id = cursor.getInt(cursor.getColumnIndex(City.Table.ID)),
                name = cursor.getString(cursor.getColumnIndex(City.Table.NAME))
        )
    }

    class SimpleDeleteResolver : DefaultDeleteResolver<City>() {
        override fun mapToDeleteQuery(entity: City) = DeleteQuery.builder()
                .table(City.Table.TABLE_NAME)
                .where("${City.Table.ID} = ${entity.id}")
                .build()
    }

    object Table {
        val TABLE_NAME = "CITIES"
        val ID = "ID"
        val NAME = "NAME"
    }
}

data class Cities(val entries: List<City>) {
    companion object {
        @JvmStatic fun deserializer() = Deserializer()
    }

    class Deserializer : JsonDeserializer<Cities> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext) = json.asJsonArray.let {
            Cities(it.map { context.deserialize<City>(it, City::class.java) })
        }
    }
}