package kode.kinopoisk.bakurov.models

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.*
import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping
import com.pushtorefresh.storio.sqlite.StorIOSQLite
import com.pushtorefresh.storio.sqlite.operations.delete.DefaultDeleteResolver
import com.pushtorefresh.storio.sqlite.operations.get.GetResolver
import com.pushtorefresh.storio.sqlite.operations.put.*
import com.pushtorefresh.storio.sqlite.queries.*
import java.lang.reflect.Type
import java.util.*

data class FilmSeances(
        //region Fields
        val cityId: Int,
        val cityName: String,
        val seanceUrl: String,
        val filmId: Int,
        val nameRu: String,
        val nameEn: String,
        val year: Int,
        val rating: String,
        val posterUrl: String,
        val filmLength: String,
        val country: String,
        val genre: List<String>,
        val videoUrl: String?,
        val items: List<Cinema>,
        val date: String?,
        val imdbId: String
        //endregion
) : Parcelable {
    companion object {
        @JvmStatic fun createTable(database: SQLiteDatabase) {
            database.execSQL("CREATE TABLE ${FilmSeances.Table.TABLE_NAME} (" +
                    "${FilmSeances.Table.FILM_ID} INTEGER PRIMARY KEY," +
                    "${FilmSeances.Table.CITY_ID} INTEGER," +
                    "${FilmSeances.Table.CITY_NAME} TEXT," +
                    "${FilmSeances.Table.SEANCE_URL} TEXT," +
                    "${FilmSeances.Table.NAME_RU} TEXT," +
                    "${FilmSeances.Table.NAME_EN} TEXT," +
                    "${FilmSeances.Table.YEAR} INTEGER," +
                    "${FilmSeances.Table.RATING} TEXT," +
                    "${FilmSeances.Table.POSTER_URL} TEXT," +
                    "${FilmSeances.Table.FILM_LENGTH} TEXT," +
                    "${FilmSeances.Table.COUNTRY} TEXT," +
                    "${FilmSeances.Table.GENRE} TEXT," +
                    "${FilmSeances.Table.VIDEO_URL} TEXT," +
                    "${FilmSeances.Table.ITEMS} TEXT," +
                    "${FilmSeances.Table.DATE} TEXT," +
                    "${FilmSeances.Table.IMDB_ID} TEXT" +
                    ");")
        }

        @JvmStatic fun storioTypeMapping(): SQLiteTypeMapping<FilmSeances> {
            return SQLiteTypeMapping.builder<FilmSeances>()
                    .putResolver(RelationsPutResolver())
                    .getResolver(RelationsGetResolver())
                    .deleteResolver(SimpleDeleteResolver())
                    .build()
        }

        @JvmStatic fun deserializer() = Deserializer()

        @JvmField val CREATOR = object : Parcelable.Creator<FilmSeances> {
            override fun createFromParcel(source: Parcel) = FilmSeances(
                    cityId = source.readInt(),
                    cityName = source.readString(),
                    seanceUrl = source.readString(),
                    filmId = source.readInt(),
                    nameRu = source.readString(),
                    nameEn = source.readString(),
                    year = source.readInt(),
                    rating = source.readString(),
                    posterUrl = source.readString(),
                    filmLength = source.readString(),
                    country = source.readString(),
                    genre = ArrayList<String>().apply { source.readStringList(this) },
                    videoUrl = source.readSerializable() as? String,
                    items = source.createTypedArrayList(Cinema.CREATOR),
                    date = source.readSerializable() as? String,
                    imdbId = source.readString()
            )

            override fun newArray(size: Int): Array<FilmSeances?> = arrayOfNulls(size)
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(cityId)
        dest.writeString(cityName)
        dest.writeString(seanceUrl)
        dest.writeInt(filmId)
        dest.writeString(nameRu)
        dest.writeString(nameEn)
        dest.writeInt(year)
        dest.writeString(rating)
        dest.writeString(posterUrl)
        dest.writeString(filmLength)
        dest.writeString(country)
        dest.writeStringList(genre)
        dest.writeSerializable(videoUrl)
        dest.writeTypedList(items)
        dest.writeSerializable(date)
        dest.writeString(imdbId)
    }

    class Deserializer : JsonDeserializer<FilmSeances> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext) = json.asJsonObject.let {
            val items = it["items"]?.asJsonArray?.map { context.deserialize<Cinema>(it, Cinema::class.java) } ?: listOf()
            FilmSeances(
                    cityId = it["cityID"].asInt,
                    cityName = it["cityName"].asString,
                    seanceUrl = it["seanceURL"].asString,
                    filmId = it["filmID"].asInt,
                    nameRu = it["nameRU"].asString,
                    nameEn = it["nameEN"].asString,
                    year = it["year"].asInt,
                    rating = it["rating"].asString,
                    posterUrl = it["posterURL"].asString,
                    filmLength = it["filmLength"].asString,
                    country = it["country"].asString,
                    genre = it["genre"].asString.split(",").map { it.trim() },
                    videoUrl = it["videoURL"]?.asString,
                    items = items,
                    date = it["date"]?.asString,
                    imdbId = it["imdbID"].asString
            )
        }
    }

    class RelationsPutResolver : PutResolver<FilmSeances>() {
        override fun performPut(storio: StorIOSQLite, model: FilmSeances): PutResult {
            val putResult = storio.put()
                    .`object`(model)
                    .withPutResolver(SimplePutResolver())
                    .prepare()
                    .executeAsBlocking()
            val itemsPutResults = storio.put()
                    .objects(model.items)
                    .prepare()
                    .executeAsBlocking()
            return PutResult.newUpdateResult(
                    putResult.numberOfRowsUpdated() ?: 0 + itemsPutResults.numberOfUpdates(),
                    setOf(FilmSeances.Table.TABLE_NAME, Cinema.Table.TABLE_NAME)
            )
        }
    }

    class SimplePutResolver : DefaultPutResolver<FilmSeances>() {
        override fun mapToInsertQuery(model: FilmSeances) = InsertQuery.builder()
                .table(FilmSeances.Table.TABLE_NAME)
                .build()

        override fun mapToUpdateQuery(model: FilmSeances) = UpdateQuery.builder()
                .table(FilmSeances.Table.TABLE_NAME)
                .where("${FilmSeances.Table.FILM_ID} = ${model.filmId}")
                .build()

        override fun mapToContentValues(model: FilmSeances) = ContentValues().apply {
            put(FilmSeances.Table.CITY_ID, model.cityId)
            put(FilmSeances.Table.CITY_NAME, model.cityName)
            put(FilmSeances.Table.SEANCE_URL, model.seanceUrl)
            put(FilmSeances.Table.FILM_ID, model.filmId)
            put(FilmSeances.Table.NAME_RU, model.nameRu)
            put(FilmSeances.Table.NAME_EN, model.nameEn)
            put(FilmSeances.Table.YEAR, model.year)
            put(FilmSeances.Table.RATING, model.rating)
            put(FilmSeances.Table.POSTER_URL, model.posterUrl)
            put(FilmSeances.Table.FILM_LENGTH, model.filmLength)
            put(FilmSeances.Table.COUNTRY, model.country)
            put(FilmSeances.Table.GENRE, model.genre.joinToString(","))
            put(FilmSeances.Table.VIDEO_URL, model.videoUrl)
            put(FilmSeances.Table.ITEMS, model.items.map { it.id }.joinToString(","))
            put(FilmSeances.Table.DATE, model.date)
            put(FilmSeances.Table.IMDB_ID, model.imdbId)
        }
    }

    class RelationsGetResolver : GetResolver<FilmSeances>() {
        private val storio = ThreadLocal<StorIOSQLite>()

        override fun performGet(storio: StorIOSQLite, rawQuery: RawQuery): Cursor {
            this.storio.set(storio)
            return storio.lowLevel().rawQuery(rawQuery)
        }

        override fun performGet(storio: StorIOSQLite, query: Query): Cursor {
            this.storio.set(storio)
            return storio.lowLevel().query(query)
        }

        override fun mapFromCursor(cursor: Cursor): FilmSeances {
            val itemsIds = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.ITEMS))
            val items = storio.get().get()
                    .listOfObjects(Cinema::class.java)
                    .withQuery(Query.builder()
                            .table(Cinema.Table.TABLE_NAME)
                            .where("${Cinema.Table.ID} IN ($itemsIds)")
                            .build())
                    .prepare()
                    .executeAsBlocking()

            return FilmSeances(
                    cityId = cursor.getInt(cursor.getColumnIndex(FilmSeances.Table.CITY_ID)),
                    cityName = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.CITY_NAME)),
                    seanceUrl = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.SEANCE_URL)),
                    filmId = cursor.getInt(cursor.getColumnIndex(FilmSeances.Table.FILM_ID)),
                    nameRu = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.NAME_RU)),
                    nameEn = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.NAME_EN)),
                    year = cursor.getInt(cursor.getColumnIndex(FilmSeances.Table.YEAR)),
                    rating = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.RATING)),
                    posterUrl = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.POSTER_URL)),
                    filmLength = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.FILM_LENGTH)),
                    country = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.COUNTRY)),
                    genre = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.GENRE)).split(","),
                    videoUrl = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.VIDEO_URL)),
                    items = items,
                    date = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.DATE)),
                    imdbId = cursor.getString(cursor.getColumnIndex(FilmSeances.Table.IMDB_ID))
            )
        }
    }

    class SimpleDeleteResolver : DefaultDeleteResolver<FilmSeances>() {
        override fun mapToDeleteQuery(model: FilmSeances) = DeleteQuery.builder()
                .table(FilmSeances.Table.TABLE_NAME)
                .where("${FilmSeances.Table.FILM_ID} = ${model.filmId}")
                .build()
    }

    object Table {
        val TABLE_NAME = "FILM_SEANCES"
        val CITY_ID = "CITY_ID"
        val CITY_NAME = "CITY_NAME"
        val SEANCE_URL = "SEANCE_URL"
        val FILM_ID = "FILM_ID"
        val NAME_RU = "NAME_RU"
        val NAME_EN = "NAME_EN"
        val YEAR = "YEAR"
        val RATING = "RATING"
        val POSTER_URL = "POSTER_URL"
        val FILM_LENGTH = "FILM_LENGTH"
        val COUNTRY = "COUNTRY"
        val GENRE = "GENRE"
        val VIDEO_URL = "VIDEO_URL"
        val ITEMS = "ITEMS"
        val DATE = "DATE"
        val IMDB_ID = "IMDB_ID"
    }
}