package kode.kinopoisk.bakurov.detail

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.*
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.hannesdorfmann.mosby.mvp.MvpView
import com.mikepenz.community_material_typeface_library.CommunityMaterial
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.iconics.IconicsDrawable
import com.orhanobut.hawk.Hawk
import com.orhanobut.logger.Logger
import com.pushtorefresh.storio.sqlite.queries.Query
import com.trello.rxlifecycle.kotlin.bindToLifecycle
import jp.wasabeef.glide.transformations.CropTransformation
import kode.kinopoisk.bakurov.R
import kode.kinopoisk.bakurov.common.RxMvpFragment
import kode.kinopoisk.bakurov.common.RxMvpPresenter
import kode.kinopoisk.bakurov.di.Injector
import kode.kinopoisk.bakurov.findView
import kode.kinopoisk.bakurov.models.Film
import kode.kinopoisk.bakurov.models.FilmSeances
import kode.kinopoisk.bakurov.tags.*
import org.jetbrains.anko.*
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*

class DetailFilmActivity : AppCompatActivity() {
    companion object {
        @JvmField val FILM_ID_EXTRA = "FILM_ID_EXTRA"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        UI(true) { frameLayout { id = R.id.container } }

        val fragment: Fragment? = supportFragmentManager.findFragmentByTag(FragmentTags.DETAIL_FILM)
        if (fragment == null) {
            val filmId = intent.getIntExtra(FILM_ID_EXTRA, 0)
            supportFragmentManager.beginTransaction()
                    .add(R.id.container, DetailFilmFragment.newInstance(filmId), FragmentTags.DETAIL_FILM)
                    .commit()
        }
    }
}

class DetailFilmFragment : RxMvpFragment<DetailFilmView, DetailFilmPresenter>(), DetailFilmView, SwipeRefreshLayout.OnRefreshListener {
    companion object {
        @JvmStatic fun newInstance(filmId: Int) = DetailFilmFragment().apply {
            arguments = Bundle().apply {
                putInt(DetailFilmActivity.FILM_ID_EXTRA, filmId)
            }
        }
    }

    private val adapter = FastItemAdapter<AbstractCardItem>()
    private lateinit var toolbar: Toolbar
    private lateinit var collapsingToolbar: CollapsingToolbarLayout
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var poster: ImageView

    override fun createPresenter() = DetailFilmPresenter(arguments.getInt(DetailFilmActivity.FILM_ID_EXTRA))
    override fun getLayoutRes() = R.layout.fragment_detail_film

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter.withOnClickListener { view, adapter, item, pos ->
            if (item is CinemaSeancesItem && item.cinema.lat != null && item.cinema.lon != null) {
                MapDialog.newInstance(item.cinema.lat.toDouble(), item.cinema.lon.toDouble())
                        .show(fragmentManager, "map_dialog")
            }
            false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findView<RecyclerView>(R.id.recycler_view) {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = this@DetailFilmFragment.adapter
        }

        swipeRefresh = view.findView<SwipeRefreshLayout>(R.id.swipe_refresh) {
            setColorSchemeResources(
                    R.color.md_blue_500,
                    R.color.md_red_500,
                    R.color.md_green_500,
                    R.color.md_orange_500
            )
            setOnRefreshListener(this@DetailFilmFragment)
        }

        toolbar = view.findView<Toolbar>(R.id.toolbar) {
            navigationIcon = IconicsDrawable(context)
                    .icon(CommunityMaterial.Icon.cmd_arrow_left)
                    .colorRes(R.color.md_white_1000)
                    .sizeDp(16)
            setNavigationOnClickListener { activity.onBackPressed() }
        }

        poster = view.findView<ImageView>(R.id.film_poster)

        collapsingToolbar = view.findView<CollapsingToolbarLayout>(R.id.collapsing_toolbar) {
            setExpandedTitleColor(ContextCompat.getColor(context, R.color.md_white_1000))
            setCollapsedTitleTextColor(ContextCompat.getColor(context, R.color.md_white_1000))
        }

        if (savedInstanceState == null) presenter.loadFilmInfo()
        else {
            Logger.t(LoggerTags.DETAIL_FILM).d("State restored")
            presenter.restore()
        }
    }

    override fun displayPoster(big: String, small: String?) {
        swipeRefresh.isRefreshing = false
        Glide.with(context)
                .load(big)
                .asBitmap()
                .animate(android.R.anim.fade_in)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .thumbnail(Glide.with(context)
                        .load(small)
                        .asBitmap()
                        .animate(android.R.anim.fade_in)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(CropTransformation(context), CenterCrop(context)))
                .transform(CropTransformation(context), CenterCrop(context))
                .into(poster)
    }

    override fun displayName(name: String) {
        collapsingToolbar.title = name
    }

    override fun displayItems(items: List<AbstractCardItem>) {
        adapter.add(items)
    }

    override fun onRefresh() {
        adapter.clear()
        presenter.loadFilmInfo()
    }
}

interface DetailFilmView : MvpView {
    fun displayPoster(big: String, small: String? = null)
    fun displayName(name: String)
    fun displayItems(items: List<AbstractCardItem>)
}

class DetailFilmPresenter(val filmId: Int) : RxMvpPresenter<DetailFilmView>() {
    init {
        Injector.inject(this)
    }

    fun loadFilmInfo() = concatSourcesAndDisplay(filmServerSource(), seancesServerSource())
    fun restore() = concatSourcesAndDisplay(filmDatabaseSource(), seancesDatabaseSource())

    private fun concatSourcesAndDisplay(film: Observable<Film>, seances: Observable<FilmSeances>) {
        val sharedFilm = film.share()

        sharedFilm
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    view?.displayPoster(it.posterBig, it.posterSmall)
                    view?.displayName(it.nameRu)
                }

        Observable.concat(
                sharedFilm.compose(FilmToCardsTransformer()),
                seances.compose(SeancesToCardsTransformer())
        )
                .subscribeOn(Schedulers.newThread())
                .bindToLifecycle(this)
                .collect({ ArrayList<AbstractCardItem>() }, { list, items -> list.addAll(items) })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view?.displayItems(it) }, { it.printStackTrace() })
    }

    private fun filmServerSource(): Observable<Film> {
        return kinopoiskApi.getFilm(filmId)
                .doOnNext {
                    storioPut(it)
                    Observable.just(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe {
                                view?.displayPoster(it.posterBig, it.posterSmall)
                                view?.displayName(it.nameRu)
                            }
                }
    }

    private fun filmDatabaseSource(): Observable<Film> {
        return storio.get()
                .withQuery<Film>(Query.builder()
                        .table(Film.Table.TABLE_NAME)
                        .where("${Film.Table.ID} = $filmId")
                        .build())
    }

    private fun seancesServerSource(): Observable<FilmSeances> {
        return Hawk.getObservable(HawkTags.CITY_ID, 490)
                .flatMap { kinopoiskApi.getSeances(it, filmId) }
                .doOnNext { storioPut(it) }
    }

    private fun seancesDatabaseSource(): Observable<FilmSeances> {
        return Hawk.getObservable(HawkTags.CITY_ID, 490)
                .flatMap { cityId ->
                    storio.get()
                            .withQuery<FilmSeances>(Query.builder()
                                    .table(FilmSeances.Table.TABLE_NAME)
                                    .where("${FilmSeances.Table.CITY_ID} = $cityId " +
                                            "AND ${FilmSeances.Table.FILM_ID} = $filmId")
                                    .build())
                }
    }

    private class FilmToCardsTransformer : Observable.Transformer<Film, List<AbstractCardItem>> {
        override fun call(source: Observable<Film>): Observable<List<AbstractCardItem>> = source
                .map {
                    listOfNotNull(
                            checkAndMapToCommonInfoItem(it),
                            checkAndMapToDescriptionItem(it),
                            checkAndMapToRentItem(it),
                            checkAndMapToBudgetItem(it),
                            checkAndMapToGalleryItem(it)
                    )
                }

        private fun checkAndMapToCommonInfoItem(film: Film): CommonInfoItem {
            return CommonInfoItem(
                    nameEn = film.nameEn,
                    year = film.year,
                    genres = film.genres,
                    ratingMpaa = film.ratingMPAA,
                    filmLength = film.filmLength,
                    country = film.country
            )
        }

        private fun checkAndMapToDescriptionItem(film: Film): DescriptionItem? {
            return if (film.desc != null)
                DescriptionItem(film.desc)
            else null
        }

        private fun checkAndMapToRentItem(film: Film): RentItem? {
            return if (arrayOf(film.premiereRu, film.premiereWorld, film.distributors, film.premiereWorldCountry).any { it != null })
                RentItem(
                        premiereRu = film.premiereRu,
                        premiereWorld = film.premiereWorld,
                        distributors = film.distributors,
                        premiereWorldCountry = film.premiereWorldCountry
                )
            else null
        }

        private fun checkAndMapToBudgetItem(film: Film): BudgetItem? {
            return if (arrayOf(film.budget, film.grossRu, film.grossUsa, film.grossWorld).any { it != null })
                BudgetItem(
                        budget = film.budget,
                        grossRu = film.grossRu,
                        grossUsa = film.grossUsa,
                        grossWorld = film.grossWorld
                )
            else null
        }

        private fun checkAndMapToGalleryItem(film: Film): GalleryItem? {
            return if (film.gallery != null)
                GalleryItem(film.gallery)
            else null
        }
    }

    private class SeancesToCardsTransformer : Observable.Transformer<FilmSeances, List<AbstractCardItem>> {
        override fun call(source: Observable<FilmSeances>): Observable<List<AbstractCardItem>> {
            val sharedSource = source.share()
            return source
                    .flatMapIterable { it.items }
                    .map { CinemaSeancesItem(it) }
                    .toList()
                    .map { it.sortedBy { it.cinema.name } }
                    .zipWith(sharedSource
                            .flatMapIterable { it.items }
                            .map { (it.seances?.size ?: 0) + (it.seances3D?.size ?: 0) }
                            .toList()
                            .map { it.sum() }
                            .zipWith(sharedSource) { seancesCount, filmSeances ->
                                CinemasCommonItem(
                                        cityName = filmSeances.cityName,
                                        cinemas = filmSeances.items.size,
                                        seances = seancesCount
                                )
                            }) { t, t2 -> t.toMutableList<AbstractCardItem>().apply { add(0, t2) } }
        }
    }
}

class MapDialog : DialogFragment() {
    companion object {
        @JvmStatic fun newInstance(lat: Double, lng: Double): MapDialog {
            val bottomSheet = MapDialog()
            bottomSheet.arguments = bundleOf(
                    "lat" to lat,
                    "lng" to lng
            )
            return bottomSheet
        }
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val latLng = LatLng(arguments.getDouble("lat"), arguments.getDouble("lng"))
        val mapOptions = GoogleMapOptions()
                .camera(CameraPosition.fromLatLngZoom(latLng, 13f))
                .mapType(GoogleMap.MAP_TYPE_NORMAL)
                .zoomControlsEnabled(true)
                .scrollGesturesEnabled(true)
                .compassEnabled(true)
                .zOrderOnTop(true)
        dialog.setContentView(
                dialog.context.UI {
                    frameLayout {
                        id = R.id.map_container

                        addView(MapView(context, mapOptions).apply {
                            onCreate(null)
                            onResume()
                            getMapAsync {
                                MapsInitializer.initialize(context)
                                it.addMarker(MarkerOptions().position(latLng))
                            }
                        }.lparams(matchParent, dip(400)))
                    }
                }.view)
    }
}