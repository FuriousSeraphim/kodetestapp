package kode.kinopoisk.bakurov

import android.app.Application
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.support.multidex.MultiDex
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.mikepenz.iconics.IconicsDrawable
import com.mikepenz.materialdrawer.util.*
import com.orhanobut.hawk.*
import com.orhanobut.logger.Logger
import jp.wasabeef.glide.transformations.CropTransformation
import kode.kinopoisk.bakurov.di.Injector

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        Hawk.init(this)
                .setEncryptionMethod(HawkBuilder.EncryptionMethod.MEDIUM)
                .setStorage(HawkBuilder.newSharedPrefStorage(this))
                .setLogLevel(LogLevel.FULL)
                .build()
        Logger.init("KODE").methodCount(1)
        Injector.init(this)
        DrawerImageLoader.init(object : AbstractDrawerImageLoader() {
            override fun set(imageView: ImageView, uri: Uri, placeholder: Drawable?) {
                val context = imageView.context
                Glide.with(context)
                        .load(uri)
                        .asBitmap()
                        .placeholder(placeholder)
                        .transform(CropTransformation(context), CenterCrop(context))
                        .into(imageView)
            }

            override fun cancel(imageView: ImageView) = Glide.clear(imageView)

            override fun placeholder(context: Context, tag: String): Drawable {
                if (DrawerImageLoader.Tags.PROFILE.name == tag) {
                    return DrawerUIUtils.getPlaceHolder(context)
                } else if (DrawerImageLoader.Tags.ACCOUNT_HEADER.name == tag) {
                    return IconicsDrawable(context).iconText(" ").backgroundColorRes(com.mikepenz.materialdrawer.R.color.primary).sizeDp(56)
                } else if ("customUrlItem" == tag) {
                    return IconicsDrawable(context).iconText(" ").backgroundColorRes(R.color.md_red_500).sizeDp(56)
                }
                return super.placeholder(context, tag)
            }
        })
    }

    override fun attachBaseContext(base: Context) {
        MultiDex.install(base)
        super.attachBaseContext(base)
    }
}