package kode.kinopoisk.bakurov

import android.content.res.Resources
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import android.widget.LinearLayout
import com.google.gson.Gson
import com.hannesdorfmann.mosby.mvp.MvpView
import com.mikepenz.community_material_typeface_library.CommunityMaterial
import com.mikepenz.materialdrawer.*
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import com.orhanobut.hawk.Hawk
import com.orhanobut.logger.Logger
import com.pushtorefresh.storio.sqlite.queries.Query
import kode.kinopoisk.bakurov.change.city.ChangeCityDialog
import kode.kinopoisk.bakurov.common.RxMvpActivity
import kode.kinopoisk.bakurov.common.RxMvpPresenter
import kode.kinopoisk.bakurov.di.Injector
import kode.kinopoisk.bakurov.models.Cities
import kode.kinopoisk.bakurov.models.City
import kode.kinopoisk.bakurov.tags.FragmentTags
import kode.kinopoisk.bakurov.tags.HawkTags
import kode.kinopoisk.bakurov.tags.LoggerTags.STORIO
import kode.kinopoisk.bakurov.today.films.TodayFilmsFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.InputStreamReader
import javax.inject.Inject

class MainActivity : RxMvpActivity<MainActivityView, MainActivityPresenter>(), MainActivityView {
    lateinit var toolbar: Toolbar private set
    private lateinit var drawerHeader: AccountHeader
    private lateinit var drawer: Drawer

    override fun createPresenter() = MainActivityPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        UI(true) {
            linearLayout {
                orientation = LinearLayout.VERTICAL

                toolbar = toolbar {
                    id = R.id.toolbar
                    backgroundResource = R.color.primary
                    minimumHeight = dimen(R.dimen.abc_action_bar_default_height_material)
                    setTitleTextColor(ContextCompat.getColor(context, R.color.md_white_1000))
                }.lparams(width = matchParent, height = wrapContent)

                frameLayout {
                    id = R.id.container
                }.lparams(width = matchParent, height = matchParent)
            }
        }

        if (!Hawk.contains(HawkTags.CITY_ID)) {
            presenter.putCityToHawk(City(490, "Калининград"))
            presenter.storeCitiesToDB(resources)
        }

        createDrawer()

        val todayFilmsFragment: Fragment? = supportFragmentManager.findFragmentByTag(FragmentTags.TODAY_FILMS)
        if (todayFilmsFragment == null)
            supportFragmentManager.beginTransaction()
                    .add(R.id.container, TodayFilmsFragment(), FragmentTags.TODAY_FILMS)
                    .commit()
    }

    private fun createDrawer() {
        val cityName = Hawk.get<String>(HawkTags.CITY_NAME)
        drawerHeader = AccountHeaderBuilder()
                .withActivity(this)
                .withSelectionListEnabledForSingleProfile(false)
                .withHeaderBackground(R.drawable.account_background)
                .addProfiles(ProfileDrawerItem()
                        .withName(cityName)
                        .withEmail("KODE Kinopoisk test app")
                        .withIcon(R.drawable.drawer_icon))
                .build()
        drawer = DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(drawerHeader)
                .addDrawerItems(
                        PrimaryDrawerItem().withName(R.string.today_films).withIcon(CommunityMaterial.Icon.cmd_theater),
                        PrimaryDrawerItem().withName(R.string.change_city).withIcon(CommunityMaterial.Icon.cmd_map_marker)
                                .withOnDrawerItemClickListener { view, pos, item ->
                                    drawer.closeDrawer()
                                    drawer.setSelectionAtPosition(0)
                                    presenter.fetchCitiesAndShow()
                                    false
                                }
                )
                .withSelectedItem(0)
                .build()
    }

    override fun showCitiesDialog(cities: List<City>) {
        ChangeCityDialog.newInstance(cities)
                .onSelect {
                    presenter.putCityToHawk(it)
                    Snackbar.make(toolbar, "Славный город ${it.name}!", Snackbar.LENGTH_SHORT).show()
                }
                .show(supportFragmentManager, FragmentTags.CHANGE_CITY)
    }
}

interface MainActivityView : MvpView {
    fun showCitiesDialog(cities: List<City>)
}

class MainActivityPresenter : RxMvpPresenter<MainActivityView>() {
    @Inject lateinit var gson: Gson

    init {
        Injector.inject(this)
    }

    fun storeCitiesToDB(resources: Resources) {
        Observable.just(resources)
                .subscribeOn(Schedulers.newThread())
                .map { gson.fromJson(InputStreamReader(resources.openRawResource(R.raw.cities)), Cities::class.java) }
                .flatMap {
                    storio.put()
                            .objects(it.entries)
                            .prepare()
                            .asRxObservable()
                }
                .subscribe({
                    Logger.t(STORIO).d("Cities saved to db")
                }, {
                    Logger.t(STORIO).e(it, "Cities save fail")
                    it.printStackTrace()
                })
    }

    fun fetchCitiesAndShow() {
        storio.get()
                .listWithQuery<City>(Query.builder()
                        .table(City.Table.TABLE_NAME)
                        .build())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view?.showCitiesDialog(it) }, { it.printStackTrace() })
    }

    fun putCityToHawk(city: City) {
        Hawk.chain()
                .put(HawkTags.CITY_ID, city.id)
                .put(HawkTags.CITY_NAME, city.name)
                .commit()
    }
}