package kode.kinopoisk.bakurov.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import kode.kinopoisk.bakurov.KinopoiskApi
import kode.kinopoisk.bakurov.models.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.schedulers.Schedulers
import javax.inject.Singleton

@Module class NetworkModule {
    @Singleton @Provides fun provideOkHttp() = OkHttpClient()

    @Singleton @Provides fun provideGson(): Gson = GsonBuilder()
            .registerTypeAdapter(City::class.java, City.deserializer())
            .registerTypeAdapter(Cities::class.java, Cities.deserializer())
            .registerTypeAdapter(Film::class.java, Film.deserializer())
            .registerTypeAdapter(Films::class.java, Films.deserializer())
            .registerTypeAdapter(Profile::class.java, Profile.deserializer())
            .registerTypeAdapter(Cinema::class.java, Cinema.deserializer())
            .registerTypeAdapter(FilmSeances::class.java, FilmSeances.deserializer())
            .create()

    @Singleton @Provides fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
            .baseUrl("http://api.kinopoisk.cf/")
            .client(okHttpClient)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.newThread()))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    @Singleton @Provides internal fun providesKinopoiskApi(retrofit: Retrofit) = retrofit.create(KinopoiskApi::class.java)
}