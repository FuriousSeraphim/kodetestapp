package kode.kinopoisk.bakurov.di

import dagger.Component
import kode.kinopoisk.bakurov.MainActivityPresenter
import kode.kinopoisk.bakurov.detail.DetailFilmPresenter
import kode.kinopoisk.bakurov.today.films.TodayFilmsPresenter
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, DatabaseModule::class, NetworkModule::class))
interface MainComponent {
    fun inject(presenter: TodayFilmsPresenter)
    fun inject(presenter: DetailFilmPresenter)
    fun inject(presenter: MainActivityPresenter)
}