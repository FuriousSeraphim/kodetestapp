package kode.kinopoisk.bakurov.tags

object LoggerTags {
    val STORIO = "StorIO"
    val TODAY_FILMS = "TodayFilms"
    val DETAIL_FILM = "DetailFilm"
}