package kode.kinopoisk.bakurov.tags

object FragmentTags {
    val DETAIL_FILM = "DETAIL_FILM"
    val TODAY_FILMS = "TODAY_FILMS"
    val CHANGE_CITY = "CHANGE_CITY"
}