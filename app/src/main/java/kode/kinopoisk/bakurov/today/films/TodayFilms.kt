package kode.kinopoisk.bakurov.today.films

import android.app.Dialog
import android.graphics.Rect
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.*
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.*
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.hannesdorfmann.mosby.mvp.MvpView
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.utils.FastAdapterUIUtils
import com.mikepenz.fastadapter.utils.ViewHolderFactory
import com.mikepenz.materialize.util.UIUtils
import com.orhanobut.logger.Logger
import com.pushtorefresh.storio.sqlite.queries.Query
import com.trello.rxlifecycle.kotlin.bindToLifecycle
import jp.wasabeef.glide.transformations.BlurTransformation
import jp.wasabeef.glide.transformations.CropTransformation
import kode.kinopoisk.bakurov.*
import kode.kinopoisk.bakurov.common.RxMvpFragment
import kode.kinopoisk.bakurov.common.RxMvpPresenter
import kode.kinopoisk.bakurov.detail.DetailFilmActivity
import kode.kinopoisk.bakurov.di.Injector
import kode.kinopoisk.bakurov.models.Film
import kode.kinopoisk.bakurov.tags.LoggerTags.STORIO
import kode.kinopoisk.bakurov.tags.LoggerTags.TODAY_FILMS
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Func2
import rx.schedulers.Schedulers
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

class TodayFilmsFragment : RxMvpFragment<TodayFilmsView, TodayFilmsPresenter>(), TodayFilmsView, Toolbar.OnMenuItemClickListener {
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private val adapter = FastItemAdapter<TodayFilmsItem>()

    override fun createPresenter() = TodayFilmsPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null)
            adapter.set(savedInstanceState
                    .getParcelableArrayList<Film>("films")
                    .map { TodayFilmsItem(it) })
        adapter.withOnClickListener { view, adapter, item, pos ->
            startActivity<DetailFilmActivity> { putExtra(DetailFilmActivity.FILM_ID_EXTRA, item.film.id) }
            false
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList("films", ArrayList(adapter.adapterItems.map { it.film }))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return UI {
            frameLayout {
                swipeRefresh = swipeRefreshLayout {
                    setOnRefreshListener { presenter.loadTodayFilms() }
                    setColorSchemeResources(
                            R.color.md_blue_500,
                            R.color.md_red_500,
                            R.color.md_orange_500,
                            R.color.md_green_500
                    )
                    recyclerView = recyclerView {
                        layoutManager = GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false)
                        addItemDecoration(GridItemDecoration(8.toDp(context)))
                        adapter = this@TodayFilmsFragment.adapter
                    }.lparams(matchParent, matchParent)
                }.lparams(matchParent, matchParent)
            }
        }.view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).toolbar.apply {
            setTitle(R.string.today_films)
            inflateMenu(R.menu.today_films_menu)
            setOnMenuItemClickListener(this@TodayFilmsFragment)
        }

        if (savedInstanceState == null) presenter.loadTodayFilms()
        else Logger.t(TODAY_FILMS).d("State restored")
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        if (item.itemId == R.id.filter) presenter.fetchGenresAndShowFilter()
        return false
    }

    override fun displayItems(items: List<TodayFilmsItem>) {
        adapter.set(items)
        swipeRefresh.isRefreshing = false
    }

    override fun showFilter(genres: List<String>, lastSelected: List<String>) {
        FilterBottomSheet.newInstance(genres, lastSelected)
                .onApplyFilter { sortType, selectedGenres ->
                    Logger.d("Sort Type: $sortType\nSelected Genres: $selectedGenres")
                    presenter.filterAndSort(sortType, selectedGenres)
                }
                .show(activity.supportFragmentManager, "filter")
    }
}

interface TodayFilmsView : MvpView {
    fun displayItems(items: List<TodayFilmsItem>)
    fun showFilter(genres: List<String>, lastSelected: List<String>)
}

class TodayFilmsPresenter : RxMvpPresenter<TodayFilmsView>() {
    private val lastSelectedGenres = mutableListOf<String>()

    init {
        Injector.inject(this)
    }

    fun loadTodayFilms() {
        val todayFilmsObservable = kinopoiskApi.getTodayFilms()
                .bindToLifecycle(this)
                .doOnNext { Logger.t(TODAY_FILMS).d("Loaded ${it.films.size}") }
                .share()
        todayFilmsObservable
                .flatMapIterable { it.films }
                .map { TodayFilmsItem(it) }
                .toSortedList(descSortFunc())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view?.displayItems(it)
                }, {
                    Logger.e(it, "Fail")
                    it.printStackTrace()
                })
        todayFilmsObservable
                .flatMap { storio.put().objects(it.films).prepare().asRxObservable() }
                .subscribe({
                    Logger.t(STORIO).d("Successfully inserted ${it.numberOfInserts()} & updated ${it.numberOfUpdates()}")
                }, {
                    Logger.t(STORIO).e(it, "Fail")
                    it.printStackTrace()
                })
    }

    fun filterAndSort(sortType: SortType, selectedGenres: List<String>) {
        storio.get()
                .listWithQuery<Film>(Query.builder().table(Film.Table.TABLE_NAME).build())
                .bindToLifecycle(this)
                .observeOn(Schedulers.computation())
                .flatMapIterable { it }
                .filter { it.genres.any { selectedGenres.contains(it) } }
                .map { TodayFilmsItem(it) }
                .toSortedList(if (sortType == SortType.DESC) descSortFunc() else ascSortFunc())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    lastSelectedGenres.clear()
                    lastSelectedGenres.addAll(selectedGenres)
                    view?.displayItems(it)
                }, {
                    Logger.e(it, "Fail")
                    it.printStackTrace()
                })
    }

    fun fetchGenresAndShowFilter() {
        storio.get()
                .listWithQuery<Film>(Query.builder().table(Film.Table.TABLE_NAME).build())
                .observeOn(Schedulers.computation())
                .flatMapIterable { it }
                .collect({ LinkedHashSet<String>() }) { set, film -> set.addAll(film.genres) }
                .map { it.toList().sorted() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view?.showFilter(it, lastSelectedGenres) }, { it.printStackTrace() })
    }

    private fun descSortFunc(): Func2<TodayFilmsItem, TodayFilmsItem, Int> {
        return Func2 { first, second ->
            if (first.film.rating ?: 0f > second.film.rating ?: 0f) -1
            else if (first.film.rating ?: 0f < second.film.rating ?: 0f) 1
            else 0
        }
    }

    private fun ascSortFunc(): Func2<TodayFilmsItem, TodayFilmsItem, Int> {
        return Func2 { first, second ->
            if (first.film.rating ?: 0f > second.film.rating ?: 0f) 1
            else if (first.film.rating ?: 0f < second.film.rating ?: 0f) -1
            else 0
        }
    }
}

class TodayFilmsItem(val film: Film) : AbstractItem<TodayFilmsItem, TodayFilmsItem.Holder>() {
    companion object {
        private val factory = Factory()
        private val formatter = DecimalFormat("###,###", DecimalFormatSymbols().apply { groupingSeparator = ' ' })
    }

    override fun getType() = R.id.today_films_item_id
    override fun getLayoutRes() = 0
    override fun getFactory(): ViewHolderFactory<out Holder> = Companion.factory

    override fun getViewHolder(parent: ViewGroup): Holder {
        return super.getViewHolder(parent.context.UI {
            cardView {
                layoutParams = ViewGroup.LayoutParams(matchParent, dip(170))
                radius = dip(4).toFloat()
                cardElevation = dip(4).toFloat()

                frameLayout {
                    layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
                    UIUtils.setBackground(this, FastAdapterUIUtils.getSelectableBackground(
                            context,
                            ContextCompat.getColor(context, R.color.md_grey_500),
                            true
                    ))

                    imageView {
                        id = R.id.preview
                        scaleType = ImageView.ScaleType.CENTER_CROP
                    }.lparams(width = matchParent, height = matchParent)

                    linearLayout {
                        orientation = LinearLayout.VERTICAL
                        backgroundResource = R.drawable.semi_transparent_label_background

                        textView {
                            id = R.id.name
                            textColor = ContextCompat.getColor(context, R.color.md_white_1000)
                            maxLines = 2
                            ellipsize = TextUtils.TruncateAt.END
                        }.lparams(width = matchParent, height = wrapContent) {
                            horizontalMargin = dip(4)
                            verticalMargin = dip(4)
                        }

                        textView {
                            id = R.id.rating
                            textColor = ContextCompat.getColor(context, R.color.md_grey_300)
                            textSizeDimen = R.dimen.today_film_item_rating_text_size
                            maxLines = 1
                            ellipsize = TextUtils.TruncateAt.END
                        }.lparams(width = matchParent, height = wrapContent) {
                            horizontalMargin = dip(4)
                            bottomMargin = dip(4)
                        }
                    }.lparams(width = matchParent, height = wrapContent) {
                        gravity = Gravity.BOTTOM
                    }
                }
            }
        }.view)
    }

    override fun bindView(holder: Holder) {
        super.bindView(holder)
        val context = holder.preview.context
        Glide.with(context)
                .load(film.posterBig)
                .asBitmap()
                .placeholder(R.color.md_grey_400)
                .thumbnail(
                        Glide.with(context)
                                .load(film.posterSmall)
                                .asBitmap()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .transform(CropTransformation(context), BlurTransformation(context, 16))
                )
                .animate(android.R.anim.fade_in)
                .transform(CropTransformation(context))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.preview)
        holder.name.text = film.nameRu

        val ratingVoteCount = film.ratingVoteCount?.let { " (${formatter.format(it)})" } ?: ""
        holder.rating.text = film.rating?.toString()?.plus(ratingVoteCount) ?: "-"
    }

    class Factory : ViewHolderFactory<Holder> {
        override fun create(v: View) = Holder(v)
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val preview = itemView.findView<ImageView>(R.id.preview)
        val name = itemView.findView<TextView>(R.id.name)
        val rating = itemView.findView<TextView>(R.id.rating)
    }
}

class GridItemDecoration(private val sizeGridSpacingPx: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.top = sizeGridSpacingPx / 2
        outRect.bottom = sizeGridSpacingPx / 2
        outRect.left = sizeGridSpacingPx / 2
        outRect.right = sizeGridSpacingPx / 2
    }
}

class FilterBottomSheet : BottomSheetDialogFragment() {
    companion object {
        @JvmStatic fun newInstance(genres: List<String>, currentGenres: List<String>): FilterBottomSheet {
            val bottomSheet = FilterBottomSheet()
            bottomSheet.arguments = Bundle().apply {
                putStringArrayList("genres", ArrayList(genres))
                putStringArrayList("currentGenres", ArrayList(currentGenres))
            }
            return bottomSheet
        }
    }

    private var action: ((SortType, List<String>) -> Unit)? = null

    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        val genres: List<String> = arguments.getStringArrayList("genres")
        val currentGenres: List<String> = arguments.getStringArrayList("currentGenres")

        val view = View.inflate(context, R.layout.filter_bottom_sheet, null)
        val sortTypesContainer = view.findView<RadioGroup>(R.id.sort_types_container)
        val genresCheckboxes = genres.map {
            val checkBox = CheckBox(context)
            checkBox.text = it
            checkBox.isChecked = currentGenres.contains(it)
            checkBox
        }

        view.findView<ViewGroup>(R.id.genres_container) { genresCheckboxes.forEach { addView(it) } }
        view.findView<Button>(R.id.cancel) { setOnClickListener { this@FilterBottomSheet.dismiss() } }
        view.findView<Button>(R.id.apply) {
            setOnClickListener {
                val sortType = if (sortTypesContainer.checkedRadioButtonId == R.id.sort_desc) SortType.DESC else SortType.ASC
                val selectedGenres = genresCheckboxes.filter { it.isChecked }.map { it.text.toString() }
                action?.invoke(sortType, selectedGenres)
                dismiss()
            }
        }

        dialog.setContentView(view)
    }

    fun onApplyFilter(action: (SortType, List<String>) -> Unit): FilterBottomSheet {
        this.action = action
        return this
    }
}

enum class SortType { DESC, ASC }